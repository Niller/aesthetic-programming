# Aesthetic Programming 2019 @ Aarhus University
**Course title:** Aesthetic Programming (20 ECTS), 2019  
**Name:** Winnie Soon (wsoon@cc.au.dk)  
**Time:** Every Tue 0800 – 1100 (3 hours)  
**Location:** 5361-135, unless otherwise stated

!NB:<br>
**Tutorial:** Every Wed 14.00-16.00 @ 5524-147 or 5524-139, conducted by Ann Karring and Ester Marie Aagard

**Optional shutup and code:** Wk 6-10, Friday 10.00-14.00 @ 5361 - 144, conducted by Ann Karring and Ester Marie Aagard

class detail: Groups in [etherpad](https://etherpad.net/p/ap2019)

discussion forum: https://padlet.com/wsoon/aestheticprogramming

## OUTLINE:  
“[Aesthetic Programming](http://darc.au.dk/research/research-agenda/aesthetic-programming/)” is a practice-oriented course requires no prior programming experience but with an interest in using code to explore the relationship between art, design, technology and culture within the context of software studies. The course introduces computer coding as an aesthetic, expressive, creative and critical endeavour beyond its functional application. It explores coding as a practice of reading, writing and building, as well as thinking with and in the world, and understanding the complex computational procedures that underwrite our experiences and realities in digital culture. Through coding practice, students are able to contextualize, conceptualize, articulate, design, build, write and run a piece of software. Emphasis is placed on the student acquiring practical skills of expression through an introduction to programming, and this course uses [P5.js](https://p5js.org/) primarily, which serves as a foundation for further courses on Digital Design.   

The course is designed to complement the parallel course in SOFTWARE STUDIES where further critical work will be developed and expanded but without losing sight of coding as critical work in itself. Examples of artists, designers and computer scientists will be introduced that work with code as their expressive material.   

## TASKS and Expectation (prerequisite for Oral Exam-A)
1. Read all the assigned reading and watch all the instructional video before coming to the class.
2. Do all the weekly mini exercises (mostly individual with a few group works)
3. Provide weekly peer feedback online
3. Peer-tutoring in a group format: within 20 mins in-class presentation with respondents
4. Active participation in class/instructor discussion and exercises
5. Submission of the final group project - in the form of a “readme” and a “runme” (software) packaged + in class presentation

!NB: 20 ECTS is equivalent to around 25 hours per week, including lecture and tutorial. As such, you are required to spend around 20 hours per week in reading and programming. The more you practice everyday, the more you get out of the course. There is no short cut and the best way is to PRACTICE!

!NB: Oral exam (Date: Jun 2019 - TBC)

## Other learning support environment:
1. Weekly 2 hours tutorial session (every Wed 14.00-16.00)
2. Weekly 4 hours shut up+code or code study group discussion (every Friday 10.00-14.00 from week 6-10)

## LEARNING OUTCOMES:
1. Demonstrate the ability to read and write computer code
2. Demonstrate the ability to design and program/develop computational artefacts based on executing fundamental programming concepts
3. Integrate practical programming skills and conceptual elements to contextualize and articulate computational artefacts
4. Demonstrate the understanding of Aesthetic Programming as a reflective and critical practice (a practice-based method) which allows students to explore, think and understand contemporary digital culture

## CLASS SCHEDULE:

#### Class 01 @ 💡 5361-144, 0800-12.00 | Week 6 | 5 Feb 2019: Getting Started
##### With Wed tutorial session and Fri shutup and code session.

- **Read Conceptual Materials**
  - Montfort, Nick. *Exploratory Programming For the Arts and Humanities*. MIT Press, 2016. 267-277 (Appendix A: Why Program?)
  - Vee, Annette. *Coding Literacty: How Computer Programming Is Changing Writing*. MIT Press, 2017.43-93 (Ch.1 Coding for Everyone and the Legacy of Mass Literacy)
- **Watch Instructional Video/Source**
  - [30 mins Video lecture on Learning While Making P5 JS by Lauren McCarthay. OPENVIS Conference, 2015.](https://www.youtube.com/watch?v=1k3X4DLDHdc)
  - [Short p5.js video by Daniel Shiffman](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)(watch 1.1 and 1.2)
  - [p5.js: Get Started](https://p5js.org/get-started/)
  - [Gitlab web editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)
- **Suggested Reading**
  - Lennon, Brian."[JavaScript Affogato: Programming a Culture of Improvised Expertise](https://muse.jhu.edu/article/685007/pdf)". *Configurations*, Volume 26, Number 1, Winter 2018, pp. 47-72
- **Prepare this before class:**
  - Why do you think you need to know programming? Why is it important to know programming in the area of digital design?
- **In-class structure:**
  - Why we need to learn programming?
  - Syllabus, class and learning, expectation
  - What is and why p5.js?
  - Setting up: Gitlab + p5.js lib + Atom + reading syntax
  - Run the first p5.js program
  - Mini-ex walk-through
  - **[PCD @ Aarhus](https://www.pcdaarhus.net/)** on 9 Feb (SAT) - Mandatory
- **Weekly mini ex1: due week 7, Monday night | Think About My First Program**
  - check mini_ex folder > [mini_ex1.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)
---
#### Class 02 | Week 7 | 12 Feb 2019: Geometric Shapes
##### With Wed tutorial session and Fri shutup and code session.
- **Read Conceptual Materials**
  - Abbing, R.R, Pierrot, P and Snelting, F., "[Modifying the Universal](http://www.data-browser.net/db06.html)."*Executing Practices*. Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver. Open Humanities Press, 2018, pp. 35-51
  - OR [Modifying the Universal](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY) (1 hr 15 mins video) by Femke Snelting
- **Watch Instructional Video/Source**
  - [p5.js Simple Shapes](https://p5js.org/examples/hello-p5-simple-shapes.html)
  - [Short p5.js video by Daniel Shiffman](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)(watch 1.3, 1.4, 2.1, 2.2)
- **In-Class structure:**
    - Project showcase: [Multi](http://o-r-g.com/apps/multi) by David Reinfurt
    - Basics Structure: HTML, CSS, JS
    - coordination, variables, geometry, shapes/patterns, color
    - Geometric emoji
- **Weekly mini ex2: due week 8, Monday night | Geometric emoji**
  - check mini_ex folder > [mini_ex2.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)
---
#### Class 03 | Week 8 | 19 Feb 2019: Infinite Loops
##### With Wed tutorial session and Fri shutup and code session.
- **Read Conceptual Materials**
  - Soon, Winnie. Throbber: Executing Micro-temporal Streams, 2019, forthcoming/in press. (check blackboard)
- **Watch Instructional Video/Source**
    - [Short p5.js video by Daniel Shiffman](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA) (watch 3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2)
- **Suggested Reading**
    - Farman, Jason. [Fidget Spinners](http://reallifemag.com/fidget-spinners/). *Real Life*, 2017.
- **In-Class structure:**
    - Project showcase: Asterisk Painting by John P. Bell, ported to p5.js and modified by Winnie Soon
    - Arrays
    - Conditional Statements
    - Iterations: For and While Loops
    - Time related syntax:
        - FrameRate(), FrameCount, setInterval(), millis()
    - Peer-tutoring: push/pop()
    - In-class ex: Decoding a throbber
- **Peer-tutoring: Group 1 / Respondents: Group 2, Topic: push()/pop()**
  - see [ref1](http://genekogan.com/code/p5js-transformations/) and [ref2](https://www.youtube.com/watch?v=E4RyStef-gY&index=109&list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH)
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex3: due week 9, Monday night | Designing a throbber differently**
  - check mini_ex folder > [mini_ex3.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 04 | Week 9 | 26 Feb 2019: Data capture
##### With Wed tutorial session @ 💡5524 - 147 and Fri shutup and code session.
- **Read Conceptual Materials**
    - (a short one) Pold, Søren. "Button." *Software Studies\ a lexicon*. Eds. Matthew Fuller. MIT Press, 2008. 31-36. (can access via e-library)
    - Gerlitz, Carolin, and Helmond, Anne. “The like Economy: Social Buttons and the Data-Intensive Web.” New Media & Society 15, no. 8 (December 1, 2013): 1348–65. (can access via e-library)
- **Watch Instructional Video/Source**
  - [p5.js examples - Interactivity 1](https://p5js.org/examples/hello-p5-interactivity-1.html)
  - [p5.js examples - Interactivity 2](https://p5js.org/examples/hello-p5-interactivity-2.html)
  - [p5.dom library reference](https://p5js.org/reference/#/libraries/p5.dom)
- **Suggested Reading/Instructional video**
    - playlist: [HTML/CSS/DOM- p5.js Tutorial by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX)
    - [clmtrackr - Face tracking javascript library](https://github.com/auduno/clmtrackr) by Audun M. Øygard
    - Charlton, James. [Add to Shopping Basket](http://www.aprja.net/add-to-shopping-basket/). *APRJA* 4.1 (2015)
- **In-Class structure:**
    - Project showcase: How We Act Together by Lauren McCarthy and Kyle McDonald
    - Data capture with Mouse and Keyboard events
    - Data capture with Web camera inputs: with clmtrackr.js library
    - Data capture with audio inputs: with p5.sound library
    - Peer-tutoring: p5.dom libary
- **Peer-tutoring: Group 2 / Respondents: Group 3, Topic: p5.dom library**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex4: due week 10, Monday night | CAPTURE ALL**
  - check mini_ex folder > [mini_ex4.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 05 | Week 10 | 5 Mar 2019: Pause
##### NO Wed tutorial session and NO Fri shutup and code session
##### 8.Mar.2019 > [Symposium - ScreenShots: Desire & Automated Image](https://www.facebook.com/events/384412612115375/)
- **In-Class structure:**
    - Guest lecture by [Dominique Cunin](http://dominiquecunin.acronie.org)
    - Peer-tutoring: Image and Video in p5.js
    - Discussion + reflecting Aesthetic Programming
- **Read conceptual materials**
    - Bergstrom, I. and Blackwell, A.F. (2016). The Practices of Programming. In *Proceedings of IEEE Visual Languages and Human-Centric Computing (VL/HCC) 2016*. (can access via AU e-library)
- **Peer-tutoring: Group 3 / Respondents: Group 4, Topic: Image and Video in p5.js**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex6: due week 11, Monday night | Revisit the past**
  - check mini_ex folder > [mini_ex5.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
  - *no peer-feedback this week
---
#### Class 06 | Week 11 | 12 Mar 2019: Object Orientation
##### With Wed tutorial session and Fri shutup and code session
- **Read Conceptual Materials**
  - Fuller, M & Goffey, A. "The obscure Objects of Object Orientation." *How to be a geek : essays on the culture of software*. Eds Matthew Fuller. Cambridge, UK, Malden, MA, USA : Polity, 2017
- **Watch Instructional Video/Source**
  - [Short p5.js video by Daniel Shiffman](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA) (watch 2.3, 6.1, 6.2, 6.3, 7.1, 7.2, 7.3)
  - [p5.js examples - Objects](https://p5js.org/examples/objects-objects.html)
  - [p5.js examples - Array of Objects](https://p5js.org/examples/objects-array-of-objects.html)
- **Suggested Reading/Instructional video**
    - Lee, Roger Y. *Software Engineering: A Hands-On Approach*. Springer, 2013. 17-24, 35-37 (ch. 2 Object-Oriented concepts)
    - Crutzen, Cecile and Kotkamp Erna. Object Orientation. *Software Studies\a lexicon*. Eds Matthew F. MIT Press, 2008. 200-207
    - Black, Andrew P. [Object-oriented Programming: some history, and challenges for the next fifty years](https://arxiv.org/abs/1303.0427). 2013.
    - Dahl, Ole-Johan. [The Birth of Object Orientation: the Simula Languages](https://link.springer.com/chapter/10.1007/978-3-540-39993-3_3). *Object-Orientation to Formal Methods*. Eds. Owe O., Krogdahl S., Lyche T. Lecture Notes in Computer Science, vol 2635. Springer, Berlin, Heidelberg . 2004.
    - [p5.js coding challenge #31: Flappy Bird by Daniel Shiffman](https://www.youtube.com/watch?v=cXgA1d_E-jY)
- **In-Class structure:**
    - Project showcase: [ToFu Go](http://tofu-go.com/)  by Francis Lam
    - Objects, Class, Behaviors and Arguments
    - Peer-tutoring
- **Peer-tutoring: Group 5 / Respondents: Group 6, Topic: p5.play library**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex6: due week 12, Monday night | Games with objects**
  - check mini_ex folder > [mini_ex6.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 07 | Week 12 | 19 Mar 2019: Automatisms
##### With Wed tutorial session and Fri shutup and code session (set up your own study group)
- **Read Conceptual Materials**
    - [1 hr video] [Beautiful Rules: Generative models of creativity](https://vimeo.com/26594644) (2007) by Marius Watz
    - Montfort, N, et al. "Randomness". *[10 PRINT CHR$(205.5+RND(1)); : GOTO 10](https://10print.org/)*, The MIT Press, 2012, pp. 119-146 (The chapter: Randomness)
- **Watch Instructional Video/Source**
    - take a look at different generative art sketches: http://www.generative-gestaltung.de/2/
    - The video of [Noise() vs Random()](https://www.youtube.com/watch?v=YcdldZ1E9gU) by Daniel Shiffman
- **Suggested Reading/Instructional video**
    - Langton, Chris G. (1986). "[Studying artificial life with cellular automata](https://deepblue.lib.umich.edu/bitstream/2027.42/26022/1/0000093.pdf)". Physica D: Nonlinear Phenomena. 22 (1–3): 120–149.
    - [The Game of Life (1970)](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) by John Horton Conway
    - [The Recode Project](http://recodeproject.com/) and [Memory Slam](http://nickm.com/memslam/) by Nick Montfort
    - Galanter, Philip. "[Generative Art Theory](http://cmuems.com/2016/60212/resources/galanter_generative.pdf)". *A Companion to Digital Art*. Eds. Christiane P, 2016.
    - [6 mins Video] [How to Draw with Code](https://www.youtube.com/watch?v=_8DMEHxOLQE) by Casey Reas
    - [p5.js Coding Challenge #14: Fractal Trees - Recursive by Daniel Shiffman](https://www.youtube.com/watch?v=0jjeOYMjmDU)
    - [p5.js Coding Challenge #76: Recursion by Daniel Shiffman](https://www.youtube.com/watch?v=jPsZwrV9ld0)
- **In-Class structure:**
    - Project showcase: Langton's ant (1986) by Chris Langton + [automata I](https://isohale.com/Development-1) by Catherine Griffiths
    - Rule-based and Emergent systems (revisit loops and iteration)
    - Generators and other automatisms
    - In-class ex: 10 print
    - Introduce final project brief
    - Mid way evaluation
- **Weekly mini ex7: due week 13, Monday night | A generative program**
  - check mini_ex folder > [mini_ex7.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 08 | Week 13 | 26 Mar 2019: Vocable Code
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)
- **Read Conceptual Materials**
    - Cox, Geoff and McLean, Alex. Speaking Code. MIT Press, 2013. 17-38. (ch.1 Vocable Code)
- **Watch Instructional Video/Source**
    - [p5.js reference - Typography](https://p5js.org/reference/#group-Typography) (All functions within Typography category)
    - [Text and type by Allison Parrish](https://creative-coding.decontextualize.com/text-and-type/)
- **Suggested Reading/Instructional video**
    - Rhee, Margaret. “Reflecting on Robots, Love, and Poetry.” XRDS 24, no. 2 (December 2017): 44–46.
    - [Matrix Digital Rain video tutorial by Emily Xie](https://www.youtube.com/watch?v=S1TQCi9axzg)
    - Raley, Rita. [Interferences:[Net.Writing] and the Practice of Codework](http://www.electronicbookreview.com/thread/electropoetics/net.writing). *electronic book review*, 2002.
    - Queneau, Ramond, et al. [Six Selections by the Oulipo](http://itp.nyu.edu/varwiki/uploads/six%20selections%20from%20the%20oulipo). *The New Media Reader*. Eds. Noah W-F and Nick M. The MIT Press, 2003. 147-189.
- **In-Class structure:**
    - Project showcase: Vocable Code by Winnie Soon
    - Textuality: typography, text size, alignment and style
    - Parsing JSON text files
    - Queering voices
    - Peer Tutoring
- **Peer-tutoring: Group 6 / Respondents: Group 7, Topic: JSON**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex8: due week 14, Monday night | Generate an electronic literature**
  - check mini_ex folder > [mini_ex8.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 09 | Week 14 | 2 Apr 2019: Que(e)ries and APIs
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)
- **Read Conceptual Materials**
    - [API paradigms and practices](https://firstmonday.org/ojs/index.php/fm/article/view/9553/7721) by Eric Snodgrass and Winnie Soon, 2019
- **Watch Instructional Video/Source**
    - [p5.js video tutorial: working with data by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r)(watch 10.1-10.10)
- **Suggested Reading/Instructional video**
    - Kirschenbaum, Matthew G. *Mechanisms: New Media and the Forensic Imagination*, MIT Press, 2007, 25-71.
    - Albright, Jonathan. "[The Graph API: Key Points in the Facebook and Cambridge Analytica Debacle](https://medium.com/tow-center/the-graph-api-key-points-in-the-facebook-and-cambridge-analytica-debacle-b69fe692d747)". Medium, 2018. (check out the recent hot topic around Cambridge Analytica online)
    - Bucher, Taina, “Objects of intense feeling: The case of the Twitter API”, in Computational Culture: a journal of software studies. 2013. Web. 27 Nov. 2013. <http://computationalculture.net/article/objects-of-intense-feeling-the-case-of-the-twitter-api>
 - **In-Class structure:**
    - Project showcase: Net Art Generator by Cornelia Sollfrank, updated by Winnie Soon
    - Image processing: fetching, loading and display
    - Application Programming Interfaces
    - In-class ex: Walkthrough Google API
    - Different types of errors
    - Peer Tutoring
- **Peer-tutoring: Group 7 / Respondents: Group 8, Topic: Parsing**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex9: due week 15, Monday night | Working with APIs**
  - check mini_ex folder > [mini_ex9.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 10 | Week 15 | 9 Apr 2019: Algorithms
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)
- **Read Conceptual Materials**
    - Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." *Information & Culture: A Journal of History*, vol. 51 no. 3, 2016, pp. 321-351. Project MUSE, doi:10.1353/lac.2016.0013
    - Bucher, Taina. *If...THEN: Algorithmic Power and Politics*, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms")
- **Watch Instructional Video/Source**
    - [18 mins video: Algorithms in BBC](https://www.youtube.com/watch?v=gOKVwRIyWdg) and full ver (optional) is [here](https://www.youtube.com/watch?v=T1os88EvPc4&t=0s)
    - [4 mins video: Algorithms in pseudocode and flow diagrams](https://www.youtube.com/watch?v=XDWw4Ltfy5w)
- **Suggested Reading/Instructional video**
    - Ed Finn, “What is an Algorithm,” in What Algorithms Want, MIT Press, 2017, pp. 15-56.
    - [Multiple js Files - video by Daniel Shiffman](https://www.youtube.com/watch?v=Yk18ZKvXBj4)
    - Goffey, Andrew. "Algorithm."*Software Studies\ a lexicon*. Eds. Matthew Fuller. MIT Press, 2008. pp. 15-20.
- **In-Class structure:**
    - Project showcase: The Project Formerly Known as Kindle Forkbomb Printing Press by UBERMORGEN
    - Flow chart and Algorithms
    - Peer tutoring
    - Discussion of final project
- **Peer-tutoring: Group 8 / Respondents: Group 9, Topic: How do you implement a sorting algorithm with a list/group of numbers in an array?**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex10: due week 16, Monday night | Working together with a Flow Chart**
  - check mini_ex folder > [mini_ex10.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
No class on Week 16 (16 Apr 2019) due to Easter Holiday
---
#### Class 11 | Week 17 | 23 Apr 2019: Advanced topics: Automation and Machine Learning
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)
- **Read Conceptual Materials**
    - [TED 19 mins video]: [From 1 to 100 pixels](https://www.youtube.com/watch?v=bfhcco9gS30&app=desktop) by Adam Harvey
    - [A short text] Cox, Geoff. [Machine ways of seeing](https://unthinking.photography/themes/machine-vision/ways-of-machine-seeing). unthinking photography, 2016.
- **Suggested Reading/Instructional video**
    - Dobson, James. E. ["Can An Algorithm Be Disturbed?: Machine Learning, Intrinsic Criticism, and the Digital Humanities."](https://pdfs.semanticscholar.org/987a/c2ba1da176d52036023f7ef05f47c6366d29.pdf) College Literature, vol. 42 no. 4, 2015, pp. 543-564. Project MUSE
    - [ml5js](https://ml5js.org/)
- **In-Class structure:**
  - What is Machine Learning?
  - Introducing ML js library
  - Peer-tutoring
  - Discussion on students' project
- **Peer-tutoring: Group 9 / Respondents: Group 1, Topic: node.js with 1 or 2 npm**
  - see the responsibility of peer-tutors and peer-respondents at the end of this page
- **Weekly mini ex11: due on 💡Sunday 28 Apr | Draft of Synopsis**
  - check mini_ex folder > [mini_ex11.md](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)  
---
#### Class 12+ | Week 18 | 30 Apr 2019 + 1 May 2019: Supervision
##### **We'll have two days supervision, therefore the tutorial class by Instructor will be cancelled. Details will follow soon**
---
#### No class on Week 19 (7 May 2019), work on your own synopsis
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)
---
#### Class 13 | Week 20 | 13 May 2019 @ 💡 5361-144: FINAL PROJECT PRESENTATION + Preparation for the ORAL EXAM + EVALUATION
##### NO Wed tutorial session
- Details will follow soon, and it would be a rather long day.

---
<br>

## SKETCH INSPIRATION:
- [Daily sketch in Processing](https://twitter.com/sasj_nl) by Saskia Freeke, her talk is [here: Making Everyday - GROW 2018](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww)
- [dwitter.net-within 140 characters of JS](https://www.dwitter.net)
- [zach lieberman](https://twitter.com/zachlieberman)
- [Creative Coding with Processing and P5.JS](https://www.facebook.com/groups/creativecodingp5/)
- [OpenProcessing](https://www.openprocessing.org/) (search with keywords)
- [JS1k](https://js1k.com/2018-coins/demos)

## OTHER REFERENCES:
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) by p5.js
- [Text and source code: Coding Projects with p5.js by Catherine Leung](https://cathyatseneca.gitbooks.io/coding-projects-with-p5-js/)
- [Video: Crockford on Javascript (more historical context)](https://www.youtube.com/watch?v=JxAXlJEmNMg&list=PLK2r_jmNshM9o-62zTR2toxyRlzrBsSL2)
- [A simple introduction to HTML and CSS](https://sites.google.com/sas.edu.sg/code/learn-to-code)
- McCarthy, L, Reas, C & Fry, B. *Getting Started with p5.js: Making interactive graphics in Javascript and Processing (Make)*, Maker Media, Inc, 2015.
- Shiffman, D. *Learning Processing: A Beginner’s Guide to Programming Images, Animations, and Interaction*, Morgan Kaufmann 2015 (Second Edition)
- Haverbeke,M. *[Eloquent JavaScript](https://eloquentjavascript.net/)*, 2018.
- [Video: Foundations of Programming in Javascript - p5.js Tutorial by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)

## NOTES for mini exercises:
- You are advised to spend at least 10 hours on the weekly mini exercises or coding.
- If the mini exercises are too simple, feel free to program something more complex and advance by exploring other syntax.
- It is important to address all the questions in the mini exercise, as they help you to understand the subject matter both technically and conceptually.
- Make use of your peers to learn different syntax, functions and operators and to ask questions.
- The more you practice, the more you will get out of the course.
- The aims of having weekly mini exercises are to encourage experimentation, practice programming and stimulate thinking
- Make sure the sketch folder contains necessary libraries, files and a correct directory. It should be able to run on a gitlab server through a web browser.
- In general, the mini ex needs to submit on gitlab on Monday (before the lecture), and peer-feedback should be done on Tue (before the Wed tutorial class)
- It is mandatory to submit ALL weekly mini exercises and to provide feedback to your peers. Those who fails to do any of them would not be able to attend A-Exam.

## Peer tutoring/ respondents format
-  **Within 15-20 mins presentation (peer tutoring)**
    - Teaching (able to describe/speak about specific code syntax and how it works)
    - Intergrating programming concepts and conceptual elements:
      - Relate to any of the specific theme and unpack the concepts with your syntax
      - You may also refer to the first class's assigned texts on why program/coding literacy
    - You may consider to have sample/creative code or simple code exercise for the class.  
    - You can also make use of collaborative tools e.g  code jam/live coding with teletype on Atom
    - If you plan to have slides/sample code, send them to the respondents a day before the class. i.e on Monday
- **Within 10 mins response by respondents (feedback and ask questions)**
  - Read the slides and/or sample code in advance
  - Set some border questions in mind and lead the class discussion
