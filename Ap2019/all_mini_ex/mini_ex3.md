# Weekly mini ex3: due week 9, Monday night | Designing a throbber differently

**Objective:**
- To reflect upon time and temporality in digital culture via the famous visual throbber icon.
- To experiment various computational effects of animation and transformation.

**Get some additional inspiration here:**
- Check out other works regarding a throbber:  
  - [Instant](https://www.facebook.com/ESLITE.PROJECTONE/photos/?tab=album&album_id=437623016346200) (2011) by LAI Chih-Sheng  
  - [Loading](https://festivalenter.wordpress.com/2009/04/09/electroboutique-by-alexei-shulgin-roman-minaev-aristarkh-chernyshev/) by Electroboutique:
  - [LOADING (THE BEAST 6:66/20:09)](https://www.yugo.at/processing/archive/index.php?what=loading) by Gordan Savičić
  - [Fedora’s artwork team](https://fedoraproject.org/wiki/Artwork/ArtTeamProjects/Fedora7Remix/Rhgb/Throbber) produces a series of throbber images that put emphasis on the design of spinning.


**Tasks (RUNME and README):**
1. Make sure you have read/watch the required readings/instructional videos and references
2. Study the syntaxes that have been discussed in class, and then redesign an **animated/moving** throbber.
3. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex3**. (Make sure your program can be run on a web browser)
4. Create a readme file (README.md) and upload to the same mini_ex3 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot about your program (search for how to upload a screenshot image and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/
  - **Describe** about your throbber design, both conceptually and technically.
    -  What are the time-related syntaxes/functions that you have used in your program? and why you use in this way? How is time being constructed in computation (can refer to both reading and your process of coding)?
    - Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction, what do you think a throbber tells us, and/or hides, about? How might we think about this remarkable throbber icon differently?

5. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)"  (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about, what syntaxes were used, what does the work express?
3. Do you like the design of the program, and why? and which aspect do you like the most? How would you interprete the work?
2. How about the conceptual linkage about the work beyond technical description and implementation? What's your thoughts on this?
