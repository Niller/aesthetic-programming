#### Class 01 @ 💡 5361-144, 0800-12.00 | Week 6 | 5 Feb 2019: Getting Started
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Introducing each other
2. Discussion: "why we need to learn programming?"
3. Overview: class structure, syllabus, expectation
4. What is and why p5.js?
5. Getting started: Gitlab + p5.js + Atom + syntax reading
6. Walkthrough next mini-ex1 and the concept of peer feedback
---
### 1. Introducing each other
About the Teaching team:
- [Winnie Soon](https://www.siusoon.net)
- [Ann Karring](https://www.pcdaarhus.net/about/)
- [Ester Marie Aagard](https://www.pcdaarhus.net/about/)

About you:
- Who are you?
- Do you have any programming/scripting experience? If yes, what are they?
- What are your "machine feelings" towards code?

---
### 2. Discussion: **"Why do we need to learn programming?"**

- **Montfort, Nick. *Exploratory Programming For the Arts and Humanities*. MIT Press, 2016. 267-277 (Appendix A: Why Program?)**
  - allows us to think in new ways
      - adding to the methods and perspectives and raising new questions
  - offers us a better understanding of culture and media systems, and
      - twofold: gain a better perspective and learn to develop better cultural systems.
      - Give insights into, and better analysis of, cultural systems
  - can help us improve society
     - to create and discover (p. 277)

- **Vee, Annette. *Coding Literacty: How Computer Programming Is Changing Writing*. MIT Press, 2017.43-93 (Ch.1 Coding for Everyone and the Legacy of Mass Literacy)**
  - Coding and Writing (think in terms of literacy- reading and writing) - Why is coding as an important (useful) skill for everyday life like literacy? - What are the implications that coding is framed under the umbrella of literacy and for whom?
  - Mass Programming movements: individual empowerment, new ways to think, citizenship and collective progress; employability and economic concerns
  - information accessibility (p. 47)
  - politics: literacy > ruling powers > measurement > who decides, standardizes and gets used? > pre-requisite for many other things.
  - - The rise of sharing and adaptable computational culture (p. 67) via the [People's Computer Company](http://www.digibarn.com/collections/newsletters/peoples-computer/index.html) in the 70s > free and open source
        <br><img src="http://www.digibarn.com/collections/newsletters/peoples-computer/peoples-1972-oct/1972-10-PCC-cover-medium.jpg" width="350"><br>
        .......image src: [DigiBarn Computer Museum](http://www.digibarn.com/collections/newsletters/peoples-computer/peoples-1972-oct/index.html)
  - Hobbyist culture + cheap/home computers in the 80s (p. 71) + computers as tools > education > problems of only focusing the 'use' of computers
  - Software becomes commodity separate from hardware
  - [Seymour Papert](https://www.goodreads.com/book/show/703532.Mindstorms) introduced computers as objects to think with and proposed learning through tangible examples and building (cognitive development) p. 79
  - imagination/creativity/expressivity: "literacy is not simply an isolated cognitive skills but instead gains its meaning and power in social interactions." (p. 81)

**Discuss in a group of 3 or 4 people (5 mins) - Try to synthesize/contextualize your thoughts through the text that you have read.**
  - why do we need to learn programming?
  - write your thoughts [here](https://padlet.com/wsoon/aestheticprogramming)
---
### 3. Overview: class structure, syllabus, expectation
- Forming group
- Learning outcomes
- Wed and Fri session
- Weekly themes
- Weekly mini ex and peer Feedback
- Peer-tutoring
- Expectation on the hours spent on this course
---
### 4. What is, and why, p5.js?
![Image of js](https://mdn.mozillademos.org/files/13504/execution.png)

image src: [mozilla](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)
- What is [javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript) and [p5.js](https://p5js.org/)?
    - Livescript -> Javascript (1995) by Brendan Eich, who is the co-founder of Mozilla Foundation and Corporation (see here for [more](http://cis.msjc.edu/csis125a/resources/papers-Brendan-Eich-JavaScript-Copyright-IEEE.pdf) information, a paper by Charles Severance]
        - A lightweight and interpreted language that complements Java (from novices to professional people)
    - [p5.js](https://p5js.org/) is a js library and is highly related to [Processing](https://processing.org/) (in terms of syntax, goals and visions) - a free and open source software platform for learning how to code within the context of art and design, making to code accessible for artists, designers, educators, and beginners.
- Why we learn p5.js?
  - web based for easy sharing
  - free and open Source
  - promote diversity and community outreach
  - fit the context of art and design
  - low threshold but high ceiling
  - extensibility to server based programming
---
### 5. Getting started: p5.js + Atom + Gitlab
- Setting up the coding environent
  1. Download the complete [p5.js library](https://p5js.org/download/) under Download > Complete library in a zip format > unzip it (create your working folder, and I recommend desktop)
  ![Image of p5_directory](https://gitlab.com/siusoon/aesthetic-programming/raw/master/Ap2019/class01/p5_directory.png)
  2. A web Browser (suggest [Firefox](https://www.mozilla.org/en-US/firefox/new/)), how to open the [console window](https://developer.mozilla.org/en-US/docs/Tools/Web_Console/Console_messages) (Tools > Web Developer > Web console)
  3. A Code editor - [Atom](https://atom.io/)
    - setup [atom-live-server](https://www.youtube.com/watch?v=0Xy3yDDY4IE) for live preview of code modification on a browser | Or: Go to Preference > settings > install > type atom-live-server > install. After the installation, go to menu bar > Packages > atom-live-server > start (or use mac short cut: control + Option + L)
- test the downloaded p5.js file under your newly created working folder
- **Reading [syntax](https://p5js.org/reference/)** (pay attention to Shape > 2D Primitives)
- try to change some parameters in the sketch.js file, press save. (make sure the atom-live-server has started)
- you should see the code is executed and something will display on your browser
    ![First program](https://gitlab.com/siusoon/aesthetic-programming/raw/master/Ap2019/class01/first.png)
- The basics: setup(), draw(), ellipse()
- Setting up a [Gitlab account](http://www.gitlab.com)
    - The concept of git as a distributed version control system
    - Using the [GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html) to create directories, new readme files, upload sketches etc.
    - For more advanced git users, see [here](https://gitlab.com/siusoon/Aesthetic_Programming_Book/blob/master/SettingUp.md)

---
### 6. Walkthrough next mini-ex1 and the concept of peer feedback
- **[PCD @ Aarhus](https://www.pcdaarhus.net/)** on 9 Feb (SAT) - Mandatory
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex1:
- Register a GitLab account, and form a group, and put your name + GitLab URL onto the [etherpad](https://etherpad.net/p/ap2019)
