/*Inspired by Multi by David Reinfurt
check shapes: https://p5js.org/reference/#group-Shape
sample code for class 02, prepared by Winnie Soon */

let moving_size = 60;
let static_size = 20;

function setup() {
  createCanvas(500, 600);
  frameRate(20);
}

function draw() {
  background(random(90,100));
  noStroke();
  fill(color(0,0,0));
  rect(50,110,100,26); //left rectangle

  //right eye
  rect(350,140,static_size,static_size);
  fill(color(200,100,0));
  beginShape();
  vertex(350, 160);
  vertex(343, 180);
  vertex(353, 180);
  vertex(370, 160);
  endShape(CLOSE);

  //mouth
  noFill();
  stroke(255,255,255);
  strokeWeight(2);
  ellipse(190,370,static_size,static_size);

  //moving mouse
  stroke(180);
  ellipse(mouseX, mouseY, moving_size, moving_size);
}
