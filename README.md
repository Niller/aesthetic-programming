# Aesthetic Programming

It is about the course Aesthetic Programming (BA level, 20 ECTS) as part of the Digital Design Program at Aarhus University

- [2019, Spring](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019)
- [2018, Spring](https://github.com/AUAP/AP2018)
- [2017, Spring](https://github.com/AUAP/AP2017)
- [2016, Spring](http://softwarestudies.projects.cavi.au.dk/index.php/Software_Studies_/_Aesthetic_Programming) 